{

  gROOT->LoadMacro("../source/FakeEfficiencyTool/utils/RatePlotter.cxx++");
  gROOT->ProcessLine("RatePlotter Plotter");
  gErrorIgnoreLevel = kFatal;

  Plotter.setDebug(true);
  Plotter.setPrint(true);
  Plotter.writeHistFile(true);
  Plotter.setEffDirectory("Efficiencies_Selection_2");

  Plotter.setStylePath("/nfs/dust/atlas/user/smolamic/Plot/atlasstyle-00-03-05/AtlasStyle.C");
  Plotter.setStyle(true);

  float lumi = 3219.56;
  Plotter.setLumi(lumi);

  TString dataType = Form("Data (%.1f fb^{-1})",lumi/1000.);
  TString mcProc   = "t#bar{t} MC";  

  Plotter.setLabel(mcProc.Data(),   "MC");
  Plotter.setLabel(dataType.Data(), "Data");

  std::vector<TString> sourcesMuon     = {"LF", "HF", "Tau","not_classified"};
  std::vector<TString> sourcesElectron = {"LF", "HF", "Tau", "charge_flip", "conversion", "not_classified"};
  Plotter.setFakeSourcesMuon(sourcesMuon);
  Plotter.setFakeSourcesElectron(sourcesElectron);

  Plotter.setHistRange(0.01, 1.39);
  Plotter.setRatioRange(0.1, 2.30);
  Plotter.setOutDir("/nfs/dust/atlas/user/smolamic/Code/TTHbbAnalysis/TTHbbAnalysis/VLQWbLepOffline/share/efficiency/29Jan19.ttbar");

  //Plotter.addMCFile("/atlas/cardillo/OutputGrid/Fakes-24-09-18/Merged/user.fcardill.Fake_V5.410470.e6337_e5984_s3126_r10201_r10210_p3554_output_merged.root");
  //Plotter.addMCFile("/atlas/cardillo/OutputGrid/Fakes-24-09-18/Merged/user.fcardill.Fake_V6.361106.e3601_e5984_s3126_r10201_r10210_p3531_output_merged.root");
  //Plotter.addMCFile("/atlas/cardillo/OutputGrid/Fakes-24-09-18/Merged/user.fcardill.Fake_V6.361107.e3601_e5984_s3126_r10201_r10210_p3531_output_merged.root");
  
  //TString region("Real");
  //TString region("Fake");
  
  ifstream filelist("/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.data15.Real.10Jan19.output.root.txt");
  string line;
  if (filelist.is_open()) {
      while (getline(filelist, line)) {
          Plotter.addDataFile(line.c_str());
      }
      filelist.close();
  } else {
      cout << "unable to open file." << endl;
  }

  //Plotter.addDataFile("/nfs/dust/atlas/user/smolamic/Code/FakeEfficiencyTool/run/Output/3491984/Real.root");
  //Plotter.addDataFile("/nfs/dust/atlas/user/smolamic/Code/FakeEfficiencyTool/run/Output/3491984/Fake.root");
  //Plotter.addDataFile("/nfs/dust/atlas/user/smolamic/Code/FakeEfficiencyTool/run/Fake/output.root");

  bool subtractPrompt = true;
  if(subtractPrompt){
      string filelists[] = {
        //"/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.singletop.22Jan19.txt",
        //"/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.singletop.22Jan19.txt",
        //"/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.singletop.22Jan19.onePerDSID.txt",
        //"/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.WJets.22Jan19.onePerDSID.txt",
        "/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.ttbar.22Jan19.onePerDSID.txt",
      };

        for (size_t i = 0; i<2; ++i) {
          ifstream filelist(filelists[i].c_str());

          if (filelist.is_open()) {
              while (getline(filelist, line)) {
                  Plotter.addPromptFile(line.c_str());
              }
              filelist.close();
          } else {
              cout << "unable to open file " << filelists[i] << endl;
          }
        }
  }

  Plotter.makeRatePlot2D("histo2D_Tight_el", "histo2D_Loose_el", "Real", "Data");
  Plotter.makeRatePlot2D("histo2D_Tight_mu", "histo2D_Loose_mu", "Real", "Data");

  //Plotter.makeRatePlot2D("histo2D_Tight_el", "histo2D_Loose_el", "Fake", "Data");
  //Plotter.makeRatePlot2D("histo2D_Tight_mu", "histo2D_Loose_mu", "Fake", "Data");

  //Plotter.getMCSources("el", "Loose");
  //Plotter.getMCSources("el", "Tight");
  //Plotter.getMCSources("mu", "Loose");
  //Plotter.getMCSources("mu", "Tight");

  //Plotter.makeRatePlot("histoTight_el0", "histoLoose_el0");
  //Plotter.makeRatePlot("histoTight_el1", "histoLoose_el1");

  //Plotter.makeRatePlot("histoTight_mu0", "histoLoose_mu0");
  //Plotter.makeRatePlot("histoTight_mu1", "histoLoose_mu1");

  //2D plots
  //Plotter.makeRatePlot2D("histo2D_Tight_el", "histo2D_Loose_el", "Fake", "Data");
  //Plotter.makeRatePlot2D("histo2D_Tight_mu", "histo2D_Loose_mu", "Fake", "Data");

  //Rates for different origins
  //Plotter.compareMCRates("histoTight_Fakes_electron0",    "histoLoose_Fakes_electron0",    "histoTight_HF_electron0",    "histoLoose_HF_electron0",   "red", "green");
  //Plotter.compareMCRates("histoTight_Fakes_electron1",    "histoLoose_Fakes_electron1",    "histoTight_HF_electron1",    "histoLoose_HF_electron1",   "red", "green");
  //Plotter.compareMCRates("all_histoTight_Fakes_electron", "all_histoLoose_Fakes_electron", "all_histoTight_HF_electron", "all_histoLoose_HF_electron","red", "green");

  //Plotter.compareMCRates("histoTight_HF_electron0",    "histoLoose_HF_electron0",    "histoTight_LF_electron0",    "histoLoose_LF_electron0",   "green", "blue");
  //Plotter.compareMCRates("histoTight_HF_electron1",    "histoLoose_HF_electron1",    "histoTight_LF_electron1",    "histoLoose_LF_electron1",   "green", "blue");
  //Plotter.compareMCRates("all_histoTight_HF_electron", "all_histoLoose_HF_electron", "all_histoTight_LF_electron", "all_histoLoose_LF_electron","green", "blue");

  //Plotter.compareMCRates("histoTight_Fakes_muon0",    "histoLoose_Fakes_muon0",    "histoTight_HF_muon0",    "histoLoose_HF_muon0",   "red", "green");
  //Plotter.compareMCRates("histoTight_Fakes_muon1",    "histoLoose_Fakes_muon1",    "histoTight_HF_muon1",    "histoLoose_HF_muon1",   "red", "green");
  //Plotter.compareMCRates("all_histoTight_Fakes_muon", "all_histoLoose_Fakes_muon", "all_histoTight_HF_muon", "all_histoLoose_HF_muon","red", "green");

  //Plotter.compareMCRates("histoTight_HF_muon0",    "histoLoose_HF_muon0",    "histoTight_LF_muon0",    "histoLoose_LF_muon0",    "green",  "blue");
  //Plotter.compareMCRates("histoTight_HF_muon1",    "histoLoose_HF_muon1",    "histoTight_LF_muon1",    "histoLoose_LF_muon1",    "green",  "blue");
  //Plotter.compareMCRates("all_histoTight_HF_muon", "all_histoLoose_HF_muon", "all_histoTight_LF_muon", "all_histoLoose_LF_muon", "green",  "blue");
  //
  //Plotter.compareMCRates("histoTight_HF_electron0",    "histoLoose_HF_electron0",    "histoTight_charge_flip0",    "histoLoose_charge_flip0",    "green", "violet");
  //Plotter.compareMCRates("histoTight_HF_electron1",    "histoLoose_HF_electron1",    "histoTight_charge_flip1",    "histoLoose_charge_flip1",    "green", "violet");
  //Plotter.compareMCRates("all_histoTight_HF_electron", "all_histoLoose_HF_electron", "all_histoTight_charge_flip", "all_histoLoose_charge_flip", "green", "violet");

  //Plotter.compareMCRates("histoTight_HF_muon0",    "histoLoose_HF_muon0",    "histoTight_not_classified_muon0",    "histoLoose_not_classified_muon0",    "green", "orange");
  //Plotter.compareMCRates("histoTight_HF_muon1",    "histoLoose_HF_muon1",    "histoTight_not_classified_muon1",    "histoLoose_not_classified_muon1",    "green", "orange");
  //Plotter.compareMCRates("all_histoTight_HF_muon", "all_histoLoose_HF_muon", "all_histoTight_not_classified_muon", "all_histoLoose_not_classified_muon", "green", "orange");



  //Rates for different preselections
  //std::vector< std::pair<std::string, std::string> > selections = { {"Efficiencies_Selection_1", "#geq1 jets" },
  //      							    {"Efficiencies_Selection_2", "#geq2 jets" },
  //      							    {"Efficiencies_Selection_3", "#geq3 jets" },
  //      							    {"Efficiencies_Selection_4", "#geq4 jets" } };

  //Plotter.compareSelections("histoTight_el0", "histoLoose_el0", selections, "Data");
  //Plotter.compareSelections("histoTight_el1", "histoLoose_el1", selections, "Data");

  //Plotter.compareSelections("histoTight_mu0", "histoLoose_mu0", selections, "Data");
  //Plotter.compareSelections("histoTight_mu1", "histoLoose_mu1", selections, "Data");


  //Plotter.compareSelections("histoTight_el0", "histoLoose_el0", selections, "MC");
  //Plotter.compareSelections("histoTight_el1", "histoLoose_el1", selections, "MC");

  //Plotter.compareSelections("histoTight_mu0", "histoLoose_mu0", selections, "MC");
  //Plotter.compareSelections("histoTight_mu1", "histoLoose_mu1", selections, "MC");
  
}
