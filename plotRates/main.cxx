#include <iostream>
#include <fstream>
#include "TString.h"
#include "../../source/FakeEfficiencyTool/utils/RatePlotter.h"

int usage() {
    std::cout << "Usage:\n"
        << "plotRates SELECTION LUMI FAKE_EFF REAL_EFF [PROMPT_EFF...]\n"
        << "CONFIG_FILE - config file\n"
        << "FAKE_EFF - file containing list of fake-efficiency input files\n"
        << "REAL_EFF - file containing list of real-efficiency input files\n"
        << "PROMPT_EFF - file(s) containing list(s) of prompt-efficiency input files\n"
        << std::endl;
    return 1;
}

int config() {
    std::cout << "Config file format:\n"
        << "key value\n"
        << "separated by space, one pair per line\n"
        << "keys:\n"
        << "- stylePath: path to AtlasStyle.C\n"
        << "- lumi: target lumi\n"
        << "- selection: selection directory in input root files\n"
        << std::endl;
    return 1;
}

int main(int argc, char** argv) {
    if (argc < 4) {
        std::cout << "Error: Too few arguments" << std::endl;
        return usage();
    }

    std::string selection;
    float lumi;
    std::string stylePath;
    bool hasLumi = false;
    bool hasSelection = false;
    bool hasStylePath = false;
    std::string line;
    {
        std::ifstream configfile(argv[1]);
        if (configfile.is_open()) {
            std::string key;
            std::string value;
            while (getline(configfile, line)) {
                std::stringstream ss(line);
                ss >> key;
                ss >> value;
                if (key == "lumi") {
                    lumi = atof(value.c_str());
                    hasLumi = true;
                } else if (key == "stylePath") {
                    stylePath = value;
                    hasStylePath = true;
                } else if (key == "selection") {
                    selection = value;
                    hasSelection = true;
                }
            }
            configfile.close();
        } else {
            std::cout << "Could not open config file " << argv[1] << std::endl;
            return usage();
        }
    }

    if (!hasLumi) {
        std::cout << "Missing lumi in config file" << std::endl;
        return config();
    }

    if (!hasSelection) {
        std::cout << "Missing selection in config file" << std::endl;
        return config();
    }

    if (!hasStylePath) {
        std::cout << "Missing style path in config file" << std::endl;
        return config();
    }


    std::string fake_eff(argv[2]);
    std::string real_eff(argv[3]);
    int nprompt = argc - 4;
    std::string prompt_eff[nprompt];
    for (int p = 0; p < nprompt; ++p) {
        prompt_eff[p] = std::string(argv[p + 4]);
    }

    std::vector<TString> sourcesMuon = {"LF", "HF", "Tau", "not_classified"};
    std::vector<TString> sourcesElectron = {"LF", "HF", "Tau", "charge_flip", "conversion", "not_classified"};

    const float hist_min = 0.01;
    const float hist_max = 1.39;
    const float ratio_min = 0.1;
    const float ratio_max = 2.3;

    RatePlotter fake_plotter;

    fake_plotter.setDebug(true);
    fake_plotter.setPrint(true);
    fake_plotter.writeHistFile(true);
    fake_plotter.setEffDirectory(selection.c_str());
    fake_plotter.setStylePath(stylePath.c_str());
    fake_plotter.setStyle(true);
    fake_plotter.setLumi(lumi);
    fake_plotter.setFakeSourcesMuon(sourcesMuon);
    fake_plotter.setFakeSourcesElectron(sourcesElectron);
    fake_plotter.setHistRange(hist_min, hist_max);
    fake_plotter.setRatioRange(ratio_min, ratio_max);
    fake_plotter.setOutDir(".");

    {
        std::ifstream filelist(fake_eff.c_str());
        if (filelist.is_open()) {
            while (getline(filelist, line)) {
                fake_plotter.addDataFile(line.c_str());
            }
            filelist.close();
        } else {
            std::cout << "Unable to open fake-efficiency-file " << fake_eff << std::endl;
            return usage();
        }
    }

    for (int i = 0; i < nprompt; ++i) {
        std::ifstream filelist(prompt_eff[i].c_str());
        if (filelist.is_open()) {
            while (getline(filelist, line))
                fake_plotter.addPromptFile(line.c_str());
            filelist.close();
        } else {
            std::cout << "Unable to open prompt-file " << prompt_eff[i] << std::endl;
            return usage();
        }
    }

    fake_plotter.makeRatePlot2D("histo2D_Tight_el", "histo2D_Loose_el", "Fake", "Data");
    fake_plotter.makeRatePlot2D("histo2D_Tight_mu", "histo2D_Loose_mu", "Fake", "Data");
    
    RatePlotter real_plotter;
    real_plotter.setDebug(true);
    real_plotter.setPrint(true);
    real_plotter.writeHistFile(true);
    real_plotter.setEffDirectory(selection.c_str());
    real_plotter.setStylePath("AtlasStyle.C");
    real_plotter.setStyle(true);
    real_plotter.setLumi(lumi);
    real_plotter.setFakeSourcesMuon(sourcesMuon);
    real_plotter.setFakeSourcesElectron(sourcesElectron);
    real_plotter.setHistRange(hist_min, hist_max);
    real_plotter.setRatioRange(ratio_min, ratio_max);
    real_plotter.setOutDir(".");

    {
        std::ifstream filelist(real_eff.c_str());
        if (filelist.is_open()) {
            while (getline(filelist, line)) {
                real_plotter.addDataFile(line.c_str());
            }
            filelist.close();
        } else {
            std::cout << "Unable to open real-efficiency-file " << real_eff << std::endl;
            return usage();
        }
    }

    for (int i = 0; i < nprompt; ++i) {
        std::ifstream filelist(prompt_eff[i].c_str());
        if (filelist.is_open()) {
            while (getline(filelist, line))
                fake_plotter.addPromptFile(line.c_str());
            filelist.close();
        } else {
            std::cout << "Unable to open prompt-file " << prompt_eff[i] << std::endl;
            return usage();
        }
    }
    
    real_plotter.makeRatePlot2D("histo2D_Tight_el", "histo2D_Loose_el", "Real", "Data");
    real_plotter.makeRatePlot2D("histo2D_Tight_mu", "histo2D_Loose_mu", "Real", "Data");

    std::cout << "done." << std::endl;
    return 0;
}
