# Configuration for running FakeEfficiencyTool and RatePlotter


## Structure
```
FakeEfficiencyTool
|- run (this repo)
|- source
|  |- FakeEfficiencyTool (FakeEfficiencyTool-repo)
|- build
```

## Setup
create your base directory, clone this repo:
```
git clone https://:@gitlab.cern.ch:8443/msmola/fakeefficiencytool-setup-vlqwbleptonic.git run/
```
create source dir, clone FakeEfficiencyTool
```
mkdir source
git clone https://:@gitlab.cern.ch:8443/atlas-phys/exot/lpx/dilepton/FakeEfficiencyTool.git source/FakeEfficiencyTool/
```
build FakeEfficiencyTool
```
mkdir build
cd build
sh ../source/FakeEfficiencyTool/scrips/setup.sh
```

## Run online
1. go to `FakeEfficiencyTool/run`
1. `source ../build/*/setup.sh`
1. update `userOpt.ini` with at least the right cern username
1. `python ../source/FakeEfficiencyTool/scripts/submitJobsUnified.py`
1. use the config files in Real for real rates on data, Fake for fake rates on data
and Prompt for prompt files on MC

## Plot efficiency parametrizations
update `makePlots.C` with the right filepaths (for data- and prompt-filepaths and outdir)
and run it once with the Real-rates using
```c++
  Plotter.makeRatePlot2D("histo2D_Tight_el", "histo2D_Loose_el", "Real", "Data");
  Plotter.makeRatePlot2D("histo2D_Tight_mu", "histo2D_Loose_mu", "Real", "Data");
```
and once for Fake-rates
```c++
  Plotter.makeRatePlot2D("histo2D_Tight_el", "histo2D_Loose_el", "Fake", "Data");
  Plotter.makeRatePlot2D("histo2D_Tight_mu", "histo2D_Loose_mu", "Fake", "Data");
```
before repeating these steps make sure to first delete the output Efficiency2D_Data.root
file or change output filepath in makePlots.C

## Better plotting with plotRates
WIP: compiled plotRates binary for plotting Real and Fake-rates in one go with Prompt files as command line arguments
