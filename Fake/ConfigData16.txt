LibraryNames libTopEventSelectionTools libTopEventReconstructionTools libFakeEfficiencyTool


GRLDir /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/
GRLFile data15_13TeV/20170619/physics_25ns_21.0.19.xml data16_13TeV/20170605/physics_25ns_21.0.19.xml data17_13TeV/20180309/physics_25ns_Triggerno17e33prim.xml data18_13TeV/20181111/physics_25ns_Triggerno17e33prim.xml 

# MC16a pileup profile
PRWConfigFiles_FS dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16a.FS.v2/prw.merged.root
PRWConfigFiles_AF dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16a.AF.v2/prw.merged.root
PRWCustomScaleFactor 1.0/1.03:1.0/0.99:1.0/1.07
# lumicalc for full 2015+2016 dataset (release 21, 2015:OflLumi-13TeV-008 2016:OflLumi-13TeV-009 - Fully compatible)
PRWLumiCalcFiles GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root

#NEvents 10000

# here we set a number of flags for specific functions that can be set to True or False
DynamicKeys Debug,EventWeights,RateType,doECID,Muon_d0,MuonLoose_d0,Muon_z0,MuonLoose_z0,Electron_d0,ElectronLoose_d0,Electron_z0,ElectronLoose_z0
Debug False
doECID True
EventWeights True

#RateType Real
#RateType FakeSFSS
#RateType FakeOFSS
RateType Fake1L

UseAodMetaData True
IsAFII False

ElectronCollectionName Electrons
MuonCollectionName Muons
JetCollectionName AntiKt4EMTopoJets
LargeJetCollectionName None
LargeJetSubstructure None
TauCollectionName None
PhotonCollectionName None
METCollectionName MET_Reference_AntiKt4EMTopo
TruthCollectionName TruthParticles
TruthJetCollectionName None #AntiKt4TruthJets
TruthElectronCollectionName None
TruthMuonCollectionName None
TruthMETCollectionName None
TrackJetCollectionName None

JetPt 25000.
JetEta 4.5

PDFInfo False

ObjectSelectionName top::ObjectLoaderStandardCuts
OutputFormat top::EventSaverTop
OutputEvents SelectedEvents
OutputFilename output.root

JetUncertainties_NPModel GlobalReduction
#JetUncertainties_BunchSpacing 25ns

ElectronID TightLH
ElectronIDLoose MediumLH
ElectronIsolation Gradient
ElectronIsolationLoose None
ElectronVetoLArCrack False
ElectronPt 10000.
Electron_d0 5.
Electron_z0 0.5
ElectronLoose_d0 5.
ElectronLoose_z0 0.5 
UseElectronChargeIDSelection False

MuonQuality Medium
MuonQualityLoose Loose
MuonIsolation Gradient
MuonIsolationLoose None
MuonPt 10000.
MuonEta 2.5
Muon_d0 3.
Muon_z0 0.5
MuonLoose_d0 7.
MuonLoose_z0 0.5

DoLoose Both
DoTight False

applyTTVACut False
ApplyTightSFsInLooseTree True

OverlapRemovalLeptonDef Loose
OverlapRemovalProcedure recommended

BTaggingWP FixedCutBEff_77
BTagVariableSaveList MV2c10_discriminant
Systematics Nominal


SELECTION Basic
INITIAL
GRL
GOODCALO
PRIVTX
NOBADMUON
JETCLEAN LooseBad
TRIGDEC HLT_e26_lhtight_nod0_ivarloose HLT_e60_lhmedium_nod0 HLT_e140_lhloose_nod0 HLT_mu26_ivarmedium HLT_mu50


SELECTION Basic_Real
. Basic
Z


SELECTION Basic_FakeSS
. Basic
2LSS
JET_N_BTAG FixedCutBEff_77 >= 1


SELECTION Basic_Fake1L
. Basic
1L
MET < 20000


SELECTION Data16_1
. Basic_Fake1L
JET_N 20000 >= 1
SAVE

SELECTION Data16_2
. Basic_Fake1L
JET_N 20000 >= 2
SAVE

SELECTION Data16_3
. Basic_Fake1L
JET_N 20000 >= 3
SAVE

SELECTION Data16_4
. Basic_Fake1L
JET_N 20000 >= 4
SAVE

