#!/bin/bash

# arg $1: cluster-ID
# arg $2: Fake or Real
# arg $3: Dataset Title / filelist name

# setup
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase &&
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh &&
asetup AnalysisTop,21.2.42 &&
source /nfs/dust/atlas/user/smolamic/Code/FakeEfficiencyTool/build/*/setup.sh &&

ls
echo $3
cat "${3}.txt"

top-xaod Config.txt "${3}.txt"

mkdir -p "Output/$1"
mv output.root "Output/${1}/${2}.root"
cp Config.txt "Output/${1}/Config${2}.txt"
cp "${3}.txt" "Output/${1}/${3}.txt"
